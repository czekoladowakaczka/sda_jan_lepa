/*
 * Deck.hpp
 *
 *  Created on: 25.04.2017
 *      Author: RENT
 */

#ifndef DECK_HPP_
#define DECK_HPP_
#include <ctime>
#include <cstdlib>

#include "Card.hpp"

class Deck {
public:

	Deck() :
	mCards(0), mCardCount(0), mCurrentCard(0)
	{
		srand(time(NULL));

		mCardCount = Card::ColourEnd * Card::FigureEnd;
		mCards = new Card[mCardCount];
		for (int c = 0; c < Card::ColourEnd; c++)
		{
			for (int f = 0; f < Card::FigureEnd; f++)
			{
				mCards[c * Card::FigureEnd + f].setValues((Card::Colour) c, (Card::Figure) f);
			}
		}
	}

	~Deck()
	{
		delete [] mCards;
	}

	void shuffle()
	{
		Card tempCard;

		for(int i=0; i<mCardCount; ++i)
		{
			int pos = rand()%mCardCount;
			tempCard = mCards[i];
			mCards[i]= mCards[pos];
			mCards[pos]=tempCard;
		}
	}

//	for(int i = mCardCount; i >0; i--)
//	{
//		int pos = rand()% mCardCount;
//		Card tmp = mCards[i-1];
//		mCards[i-1]=mCards[pos];
//		mCards[pos]=tmp;
//
//	}

	Card getNextCard()
	{
		Card chosenCard;
		if(mCurrentCard<mCardCount)
		{
			mCurrentCard++;
			chosenCard =  mCards[mCurrentCard-1];
		}
		else
		{
			//nic
		}
		return chosenCard;
	}

private:
	Card* mCards;
	int mCardCount;
	int mCurrentCard;
};

#endif /* DECK_HPP_ */
