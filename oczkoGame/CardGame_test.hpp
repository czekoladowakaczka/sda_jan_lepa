/*
 * CardGame_test.hpp
 *
 *  Created on: 25.04.2017
 *      Author: RENT
 */

#include "gtest/gtest.h"
#include "Deck.hpp"
#include "Card.hpp"

TEST(CardClassTest, cardCreationDefault)
{
	Card card;
	EXPECT_EQ(0, card.getValue());
	EXPECT_EQ(Card::ColourEnd, card.getColour());
	EXPECT_EQ(Card::FigureEnd, card.getFigure());
}

TEST(CardClassTest, cardSetValue)
{
	Card card;
	card.setValues(Card::Karo, Card::FIGURE_3);
	EXPECT_EQ(3, card.getValue());
	EXPECT_EQ(Card::Karo, card.getColour());
	EXPECT_EQ(Card::FIGURE_3, card.getFigure());
}

TEST(DeckClassTest, deckGetCard)
{
	Deck deck;
	Card card = deck.getNextCard();
	EXPECT_NE(0, card.getValue());
	EXPECT_NE(Card::ColourEnd, card.getColour());
	EXPECT_NE(Card::FigureEnd, card.getFigure());
}

TEST(DeckClassTest, deckGetCard52)
{
	Deck deck;
	Card card;
	for(int i=0; i<52; i++)
	{
		card = deck.getNextCard();
	}
	EXPECT_NE(0, card.getValue());
	EXPECT_NE(Card::ColourEnd, card.getColour());
	EXPECT_NE(Card::FigureEnd, card.getFigure());
}

TEST(DeckClassTest, deckGetCard53)
{
	Deck deck;
	Card card;
	for(int i=0; i<53; i++)
	{
		card = deck.getNextCard();
	}
	EXPECT_EQ(0, card.getValue());
	EXPECT_EQ(Card::ColourEnd, card.getColour());
	EXPECT_EQ(Card::FigureEnd, card.getFigure());
}

int main(int argc, char **argv) {
      ::testing::InitGoogleTest(&argc, argv);
      return RUN_ALL_TESTS();
}
