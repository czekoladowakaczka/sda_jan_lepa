/*
 * LiczbaNieujemna.hpp
 *
 *  Created on: 15.05.2017
 *      Author: RENT
 */

#ifndef LICZBANIEUJEMNA_HPP_
#define LICZBANIEUJEMNA_HPP_
#include <iostream>
#include <exception>
#include <sstream>
using namespace std;

class BladKonstrukcji : public exception
{
public:

	int blednaLiczba;

	BladKonstrukcji(int liczba)
	:blednaLiczba(liczba)
	{}

	int getBlednaLiczba()
	{
		return blednaLiczba;
	}

	virtual const char * what () const throw ()
	{
		return "Blad w konstruktorze";
	}
};

class ZleArgumenty : public exception
{
private:
	int blednaLiczba;

public:
	ZleArgumenty(int liczba)
	:blednaLiczba(liczba)
	{}

	int getBlednaLiczba()
	{
		return blednaLiczba;
	}

	const char * what () const throw ()
	{
		return "Blad argumentu";
	}
};

class LiczbaNieujemna
{
private:
	int mValue;

public:

	LiczbaNieujemna(int v)
	{
		if(v<0)
		{
			//std::string blad("Podano ujemna liczbe");
			//throw blad;

			//std::out_of_range blad("Podano ujemna liczbe");

			throw BladKonstrukcji(v);
		}
		mValue = v;
	}

	int getValue() const
	{
		return mValue;
	}

	void setValue(int value)
	{
		mValue = value;
	}

	LiczbaNieujemna operator- (LiczbaNieujemna& druga)
	{
		if(this->mValue<druga.mValue)
		{
			throw ZleArgumenty(druga.getValue());
		}
		return this->mValue-druga.mValue;
	}

	LiczbaNieujemna operator* (LiczbaNieujemna& druga)
	{
		return this->mValue*druga.mValue;
	}

	LiczbaNieujemna operator/ (LiczbaNieujemna& druga)
	{
		if (0==druga.mValue)
		{
			throw ZleArgumenty(druga.getValue());
		}
		return this->mValue/druga.mValue;
	}

	friend std::ostream& operator<< (std::ostream& stream, const LiczbaNieujemna& liczba);
};

std::ostream& operator<< (std::ostream& stream, const LiczbaNieujemna& liczba)
{
	stream << liczba.mValue;
//	liczba.wypisz(stream);
	return stream;
}


#endif /* LICZBANIEUJEMNA_HPP_ */
