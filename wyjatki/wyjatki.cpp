//============================================================================
// Name        : wyjatki.cpp
// Author      : ghhh
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
#include <exception>
#include "LiczbaNieujemna.hpp"

using namespace std;

int main()
{
	try
		{
			LiczbaNieujemna duza(20);
			LiczbaNieujemna mala(10);
			LiczbaNieujemna zero(0);
			LiczbaNieujemna jeden(1); //blad

			cout<< "mnoze " << jeden << "*" << mala << "=" << jeden * mala << endl;
			cout<< "mnoze " << duza << "*" << zero << "=" << duza * zero << endl; //blad

			try
			{
					cout << "odejmuje " << duza << "-" << mala << "=" << duza - mala << endl;
					cout << "odejmuje " << mala << "-" << duza << "=" << mala - duza << endl; //blad
			}
			catch(ZleArgumenty& str)
			{
				cout << "odejmowanie " << str.what() << endl;
			}

			try
			{
					cout << "dziele " << duza << "/" << mala << "=" << duza / mala	<< endl;
					cout << "dziele " << mala << "/" << zero << "=" ;
					cout << mala / zero << endl; //blad
			}
			catch(ZleArgumenty& str)
			{
				cout << "dzielenie " << str.what() << endl;
			}

		}
		catch (BladKonstrukcji& str)
		{
			cout << "k-tor: " << str.what() << endl;
			throw str;
		}

		return 0;
}
