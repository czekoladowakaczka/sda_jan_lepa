#ifndef DATA_HPP_
#define DATA_HPP_
#include <iostream>

class Data
{
public:
    Data();
    Data(int dzien, int miesiac, int rok);

    int getDzien() const;
    void setDzien(int dzien);
    int getMiesiac() const;
    void setMiesiac(int miesiac);
    int getRok() const;
    void setRok(int rok);
    void wypisz();
    void przesunDzien(int ileDni);
    void przesunMiesiac(int ileMiesiecy);
    void przesunRok(int ileLat);
    Data obliczRoznice(Data d);
    bool czyWieksza(Data a, Data b);

protected:
    int mDzien;
    int mMiesiac;
    int mRok;
};



#endif /* DATA_HPP_ */
