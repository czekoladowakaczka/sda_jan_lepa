
#include "Czas.hpp"
#include <iostream>


	Czas::Czas()
	:mGodziny(0)
	,mMinuty(0)
	,mSekundy(0)
	{
	}

	Czas::Czas (unsigned int godziny, unsigned int minuty,unsigned int sekundy)
	:mGodziny(godziny)
	,mMinuty(minuty)
	,mSekundy(sekundy)
	{
	}

	void Czas::podajGodzine()
	{
		std::cout<<mGodziny<<"."<<mMinuty<<"."<<mSekundy<<std::endl;
	}

	unsigned int Czas::getGodziny() const
	{
		return mGodziny;
	}

	void Czas::setGodziny(unsigned int godziny)
	{
		mGodziny = godziny;
	}

	unsigned int Czas::getMinuty() const
	{
		return mMinuty;
	}

	void Czas::setMinuty(unsigned int minuty)
	{
		mMinuty = minuty;
	}

	unsigned int Czas::getSekundy() const
	{
		return mSekundy;
	}

	void Czas::setSekundy(unsigned int sekundy)
	{
		mSekundy = sekundy;
	}

	void Czas::przesunGodzine(unsigned int godziny)
	{
		mGodziny += godziny;
		if(mGodziny>24)
		{
			mGodziny = 24;
		}
	}

	void Czas::przesunMinute(unsigned int minuty)
	{
		 if(minuty>59)
		 {
				przesunGodzine(minuty/60);
				int resztaMinut = minuty%60;
				if((mMinuty+resztaMinut)>60)
				{
				         mGodziny++;
				         mMinuty = (mMinuty+resztaMinut)%60;
				}
				else if((mMinuty+resztaMinut)<=60)
				{
				         mMinuty+=resztaMinut;

				}
		 }
				else if((mMinuty+minuty)>60)
				{
				            mGodziny++;
				            mMinuty = (mMinuty+minuty)%60;
				}
				else if((mMinuty+minuty)<=60)
				{
				            mMinuty+=minuty;
				}

	}

	void Czas::przesunSekundy(unsigned int sekundy)
	{
		 if(sekundy>59)
		       {
		            przesunMinute(sekundy/60);
		            int resztaSekund = sekundy%60;
		            if((mSekundy+resztaSekund)>60)
		            {
		                mMinuty++;
		                mSekundy = (mSekundy+resztaSekund)%60;
		            }
		            else if((mSekundy+resztaSekund)<=60)
		            {
		                mSekundy+=resztaSekund;
		            }
		        }
		        else if((mSekundy+sekundy)>60)
		        {
		            mMinuty++;
		            mSekundy = (mSekundy+sekundy)%60;
		        }
		        else if((mSekundy+sekundy)<=60)
		        {
		            mSekundy+=sekundy;
		        }
	}

	    bool Czas::czyWieksza(Czas a, Czas b)
	    {
	        bool aWieksza;

	        if(a.getGodziny()>b.getGodziny())
	        {
	            aWieksza = true;
	        }

	        else if((a.getGodziny()==b.getGodziny())&&(a.getMinuty()>b.getMinuty()))
	        {
	            aWieksza = true;
	        }

	        else if((a.getGodziny()==b.getGodziny())&&(a.getMinuty()==b.getMinuty())&&(a.getSekundy()>b.getSekundy()))
	        {
	            aWieksza = true;
	        }

	        else
	        {
	            aWieksza = false;
	        }


	        return aWieksza;
	    }


	    Czas Czas::porownajGodziny(Czas b)
	    {
	    	Czas a(mGodziny, mMinuty, mSekundy);
	    	Czas roznica;

	    	if(czyWieksza(a, b))
	    	{
	    		roznica.setGodziny(a.getGodziny()-b.getGodziny());
	    		roznica.setMinuty(a.getMinuty()-b.getMinuty());
	    		roznica.setSekundy(a.getSekundy()-b.getSekundy());
	    	}
	    	else
	    	{
	    		roznica.setGodziny(b.getGodziny()-a.getGodziny());
	    		roznica.setMinuty(b.getMinuty()-a.getMinuty());
	    		roznica.setSekundy(b.getSekundy()-a.getSekundy());
	    	}

	    	return roznica;
	    }












