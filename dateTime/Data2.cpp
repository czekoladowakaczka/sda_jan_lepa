
#include "Data2.hpp"
#include<iostream>

using namespace std;


    Data::Data()
    :mDzien(1)
    ,mMiesiac(1)
    ,mRok(1900)
    {
    }

    Data::Data(int dzien, int miesiac, int rok)
    {
        setDzien(dzien);
        setMiesiac(miesiac);
        setRok(rok);
    }
    int Data::getDzien() const
    {
        return mDzien;
    }

    void Data::setDzien(int dzien)
    {
        mDzien = dzien;
    }

    int Data::getMiesiac() const
    {
        return mMiesiac;
    }

    void Data::setMiesiac(int miesiac)
    {
        mMiesiac = miesiac;
    }

    int Data::getRok() const
    {
        return mRok;
    }

    void Data::setRok(int rok)
    {
        mRok = rok;
    }

    void Data::wypisz()
    {
        std::cout<<"Data: "<<mDzien<<"."<<mMiesiac<<"."<<mRok<<std::endl;
    }

    void Data::przesunDzien(int ileDni)
    {
        if(ileDni>31)
        {
            przesunMiesiac(ileDni/31);
            int resztaDni = ileDni%31;
            if((mDzien+resztaDni)>31)
            {
                mMiesiac++;
                mDzien = (mDzien+resztaDni)%31;
            }
            else if((mDzien+resztaDni)<=31)
            {
                mDzien+=resztaDni;
            }
        }
        else if((mDzien+ileDni)>31)
        {
            mMiesiac++;
            mDzien = (mDzien+ileDni)%31;
        }
        else if((mDzien+ileDni)<=31)
        {
            mDzien+=ileDni;
        }
    }

    void Data::przesunMiesiac(int ileMiesiecy)
    {
        if(ileMiesiecy>12)
        {
            przesunRok(ileMiesiecy/12);
            int resztaMiesiecy = ileMiesiecy%12;
            if((mMiesiac+resztaMiesiecy)>12)
            {
                    mRok++;
                    mMiesiac = (mMiesiac+resztaMiesiecy)%12;
            }
            else if((mMiesiac+resztaMiesiecy)<=12)
            {
                    mMiesiac+=resztaMiesiecy;
            }
        }
        else if((mMiesiac+ileMiesiecy)>12)
        {
            mRok++;
            mMiesiac = (mMiesiac+ileMiesiecy)%12;
        }
        else if ((mMiesiac+ileMiesiecy)<=12)
        {
            mMiesiac+=ileMiesiecy;
        }
    }

    void Data::przesunRok(int ileLat)
    {
        mRok+=ileLat;
    }

//    Data Data::obliczRoznice(Data d)
//    {
//        Data o(mDzien, mMiesiac, mRok);
//        Data roznica;
//
//        if(czyWieksza(o, d)==true)
//        {
//            roznica.setRok(o.getRok()-d.getRok());
//            if(o.getMiesiac()<d.getMiesiac())
//            {
//                roznica.setMiesiac(12-(d.getMiesiac()-o.getMiesiac()));
//                roznica.przesunRok(-1);
//            }
//            if(o.getMiesiac()>d.getMiesiac())
//            {
//            	roznica.setMiesiac(o.getMiesiac()-d.getMiesiac());
//            }
//            if(o.getDzien()<d.getDzien())
//            {
//                roznica.setDzien(31-(d.getDzien()-o.getDzien()));
//                roznica.przesunMiesiac(-1);
//            }
//
//        }
//
//        std::cout<<"Dzien "<<roznica.getDzien();
//        std::cout<<"Miesiac "<<roznica.getMiesiac();
//        std::cout<<"Rok "<<roznica.getRok();
//
//        return roznica;
//    }

//    bool Data::czyWieksza(Data a, Data b)
//    {
//        bool aWieksza;
//
//        if(a.getRok()>b.getRok())
//        {
//            aWieksza = true;
//        }
//
//        else if((a.getRok()==b.getRok())&&(a.getMiesiac()>b.getMiesiac()))
//        {
//            aWieksza = true;
//        }
//
//        else if((a.getRok()==b.getRok())&&(a.getMiesiac()==b.getMiesiac())&&(a.getDzien()>b.getDzien()))
//        {
//            aWieksza = true;
//        }
//
//        else
//        {
//            aWieksza = false;
//        }
//
//
//        return aWieksza;
//    }

