/*
 * DataCzas.cpp
 *
 *  Created on: 29.03.2017
 *      Author: RENT
 */

#include "DataCzas.hpp"

DataCzas::DataCzas()
:Data(1,1,1900)
,Czas(0,0,0)
{
}

DataCzas::DataCzas(unsigned int godziny, unsigned int minuty,unsigned int sekundy, int dzien, int miesiac, int rok)
{
	setGodziny(godziny);
	setMinuty(minuty);
	setSekundy(sekundy);
	setDzien(dzien);
	setMiesiac(miesiac);
	setRok(rok);
}

DataCzas::DataCzas(Data a, Czas b)
:Data(a.getDzien(),a.getMiesiac(), a.getRok())
,Czas(b.getGodziny(),b.getMinuty(), b.getSekundy())
{
}
