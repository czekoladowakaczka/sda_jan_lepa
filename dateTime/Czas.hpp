/*
 * Czas.hpp
 *
 *  Created on: 29.03.2017
 *      Author: RENT
 */

#ifndef CZAS_HPP_
#define CZAS_HPP_
#include <iostream>

class Czas
{
protected:
	unsigned int mGodziny;
	unsigned int mMinuty;
	unsigned int mSekundy;

public:
	Czas();
	Czas (unsigned int godziny, unsigned int minuty,unsigned int sekundy);
	void podajGodzine();
	unsigned int getGodziny() const;
	void setGodziny(unsigned int godziny);
	unsigned int getMinuty() const;
	void setMinuty(unsigned int minuty);
	unsigned int getSekundy() const;
	void setSekundy(unsigned int sekundy);
	void przesunGodzine(unsigned int godziny);
	void przesunMinute(unsigned int minuty);
	void przesunSekundy(unsigned int sekundy);
	bool czyWieksza(Czas a, Czas b);
	Czas porownajGodziny(Czas b);


};



#endif /* CZAS_HPP_ */
