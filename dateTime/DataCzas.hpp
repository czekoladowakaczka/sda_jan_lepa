/*
 * DataCzas.hpp
 *
 *  Created on: 29.03.2017
 *      Author: RENT
 */

#ifndef DATACZAS_HPP_
#define DATACZAS_HPP_
#include "Data2.hpp"
#include "Czas.hpp"

class DataCzas: public Data, Czas
{
public:
	DataCzas();
	DataCzas(unsigned int godziny, unsigned int minuty,unsigned int sekundy, int dzien, int miesiac, int rok);
	DataCzas(Data a, Czas b);
};



#endif /* DATACZAS_HPP_ */
