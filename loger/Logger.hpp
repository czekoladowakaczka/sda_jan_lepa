/*
 * Logger.hpp
 *
 *  Created on: 13.05.2017
 *      Author: RENT
 */

#ifndef LOGGER_HPP_
#define LOGGER_HPP_
#include <iostream>
#include <fstream>
using namespace std;

class FileLogger
{
private:
	ofstream mFile;
	static FileLogger* instance;
	FileLogger();

public:
//	FileLogger();

	static FileLogger* logger();

	void log(string dataToLog);
};

#endif /* LOGGER_HPP_ */
