/*
 * Logger.cpp
 *
 *  Created on: 13.05.2017
 *      Author: RENT
 */
#include "Logger.hpp"

	FileLogger* FileLogger::instance = 0;

	FileLogger::FileLogger()
	{
		mFile.open("log.txt");
	}

	void FileLogger::log(string dataToLog)
	{
		mFile<<dataToLog<<endl;
	}

	FileLogger* FileLogger::logger()
	{
		if(!instance)
		{
			instance=new FileLogger;
		}
		return instance;
	}
