/*
 * Punkt.hpp
 *
 *  Created on: 28.03.2017
 *      Author: RENT
 */

#ifndef PUNKT_HPP_
#define PUNKT_HPP_
#include <iostream>


class Punkt
{
protected:
	float mX;
	float mY;

public:
	Punkt();
	Punkt(float x, float y);
	void wypisz();
	void przesun(Punkt a);
	void przesun(float x, float y);
	void przesunX(float x);
	void przesunY(float y);
	float obliczOdleglosc(float x, float y);
	float obliczOdleglosc(Punkt a);


	float getX() const
	{
		return mX;
	}

	void setX(float x)
	{
		mX = x;
	}

	float getY() const
	{
		return mY;
	}

	void setY(float y)
	{
		mY = y;
	}
};

#endif /* PUNKT_HPP_ */
