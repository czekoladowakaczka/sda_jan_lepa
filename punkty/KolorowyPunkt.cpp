/*
 * KolorowyPunkt.cpp
 *
 *  Created on: 28.03.2017
 *      Author: RENT
 */

#include <iostream>
#include "KolorowyPunkt.hpp"

KolorowyPunkt::KolorowyPunkt(float x, float y, Color kolor)
:Punkt(x, y)
,mKolor(kolor)
{
}

void KolorowyPunkt::wypisz()
{
	Punkt::wypisz();
	switch(mKolor)
	{
	case red:
		std::cout<<"Czerwony\n";
		break;
	case blue:
		std::cout<<"Niebieski\n";
		break;
	case green:
		std::cout<<"Zielony\n";
		break;
	default:
		std::cout<<"Nieznany kolor!";
		break;
	}
}
