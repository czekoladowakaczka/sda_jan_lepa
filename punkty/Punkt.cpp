/*
 * Punkt.cpp

 *
 *  Created on: 28.03.2017
 *      Author: RENT
 */
#include <iostream>
#include <cmath>
#include "Punkt.hpp"

Punkt::Punkt()
:mX()
,mY()
{
}

Punkt::Punkt(float x, float y)
:mX(x)
,mY(y)
{
}

void Punkt::wypisz()
{
	std::cout<<"Wspolrzedne: ["<<mX<<" "<<mY<<"]"<<std::endl;
}

void Punkt::przesun(float x, float y)
{
	przesunX(x);
	przesunY(y);
}

void Punkt::przesun(Punkt a)
{
	przesun(a.getX(), a.getY());
}

void Punkt::przesunX(float x)
{
	if (mX+x>1000)
	{
			mX=1000;
	}
	else
	{
			mX += x;
	}
}
void Punkt::przesunY(float y)
{
	if (mY+y>1000)
	{
		mY=1000;
	}
	else
	{
		mY += y;
	}
}

float Punkt::obliczOdleglosc(float x, float y)
{
	return sqrt((x-mX)*(x-mX)+(y-mY)*(y-mY));
}

float Punkt::obliczOdleglosc(Punkt a)
{
	return obliczOdleglosc(a.getX(), a.getY());
}


