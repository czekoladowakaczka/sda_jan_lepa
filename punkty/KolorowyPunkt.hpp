/*
 * KolorowyPunkt.hpp
 *
 *  Created on: 28.03.2017
 *      Author: RENT
 */

#ifndef KOLOROWYPUNKT_HPP_
#define KOLOROWYPUNKT_HPP_
#include <iostream>
#include "Punkt.hpp"

class KolorowyPunkt: protected Punkt
{
public:

	enum Color
	{
		red,
		blue,
		green
	};

	KolorowyPunkt(float x, float y, Color kolor);
	void wypisz();

protected:

	Color mKolor;
};




#endif /* KOLOROWYPUNKT_HPP_ */
