/*
 * IdGenerator.hpp
 *
 *  Created on: 13.05.2017
 *      Author: RENT
 */

#ifndef IDGENERATOR_HPP_
#define IDGENERATOR_HPP_

class IdGenerator
{
private:

	static int mID;

public:

	static int getNextID();
};



#endif /* IDGENERATOR_HPP_ */
