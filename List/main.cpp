#include <iostream>
#include "List.hpp"

using namespace std;

int main()
{
    List lista1;
    lista1.addHead(1);
    lista1.addHead(2);
    lista1.addTail(3);
    lista1.addTail(4);
    lista1.addHead(-5);
    lista1.show();
    lista1.addTail(6);
    lista1.show();
    lista1.remove2(3);
    lista1.show();
    lista1.remove2(3);
    lista1.show();
    lista1.addTail(8);
    lista1.show();
    lista1.remove2(4);
    lista1.show();
    lista1.addTail(8);
    lista1.show();
    lista1.remove2(4);
    lista1.show();
    lista1.addTail(8);
    lista1.show();
    lista1.remove2(0);
    lista1.show();
    lista1.addHead(-3);
    lista1.show();
    lista1.insert(5,2);
    lista1.show();
    lista1.insert(15,0);
    lista1.insert(25,7);
    lista1.show();
    lista1.remove2(2);
    lista1.show();
    lista1.insert(5,2);
    lista1.show();

    return 0;
}
