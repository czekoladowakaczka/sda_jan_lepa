#include <iostream>

using namespace std;

class List
{
public:
    class Node
    {
        public:
        Node* next;
        Node* prev;
        int mValue;
        Node(int value)
        {
            mValue = value;
            next=0;
            prev=0;
        }
    };

private:
       Node* head;
       Node* tail;
       size_t mSize;

public:

    List()
    :head(0)
    ,tail(0)
    ,mSize(0)
    {
    }

    void addTail(int value)
    {
        if(tail!=0)
        {
            Node* newNode = new Node(value);
            newNode->prev=tail;
            tail->next = newNode;
            tail = newNode;
        }
        else
        {
            head = new Node(value);
            tail = head;
        }
        mSize++;
    }

    void addHead(int value)
    {
        if(head!=0)
        {
            Node* newNode = new Node(value);
            newNode->next = head;
            head->prev=newNode;
            head = newNode;
        }
        else
        {
            head = new Node(value);
            tail = head;
        }
        mSize++;
    }

    void show()
    {
        if(mSize!=0)
        {
            Node* tmp = head;
            cout<<"[";
            for(size_t i=0; i<mSize; ++i)
            {
                cout<<tmp->mValue<<" ";
                tmp = tmp->next;
            }
            cout<<"]"<<endl;
        }
        else
        {
            cout<<"Lista jest pusta!"<<endl;
        }
    }

    int get(const unsigned int index)
    {
        int counter=0;
        Node* tmp = 0;

        if(index<mSize/2)
        {
            tmp = head;
            while(counter!=index)
            {
                tmp=tmp->next;
                counter++;
            }
        }
        else
        {
            tmp = tail;
            while(counter!=index)
            {
                tmp=tmp->prev;
                counter++;
            }
        }
        return tmp->mValue;
    }

    int findValue(int value)
    {
        int counter=0;
        Node* tmp = head;

        while(tmp->mValue!=value)
        {
            tmp=tmp->next;
            counter++;
        }
        return counter;
    }

    bool isEmpty()
    {
        if(mSize!=0)
        {
            return false;
        }
        return true;
    }

    void clean() //funkcja usuwajace wszystkie Wezly z listy
    {
        Node* tmp = head;
        while (tmp != 0) //test czy nie jest to ostatni element
        {
            head = tmp->next;
            delete tmp;
            tmp = head; //pobierz nastepny element z listy
        }
        head = 0;
        tail = 0;
        mSize = 0;
    }

    size_t size()
    {
        return mSize;
    }

    void remove2(const int index)
    {
        if(0==index)
        {
            Node* tmp = head;
            head=head->next;
            head->prev=0;
            delete tmp;
            mSize--;
        }

        else if(index==(mSize-1))
        {
            Node* temp = tail;
            tail=tail->prev;
            tail->next=0;
            delete temp;
        }

        else
        {
            int counter = 0;
            Node* tmp = 0;

            if(index<mSize/2)
            {
                tmp=head;
                while(counter!=index)
                {
                    tmp=tmp->next;
                    counter++;
                }
                tmp->prev->next=tmp->next;
                tmp->next->prev=tmp->prev;

            }



            delete tmp;
            mSize--;
        }
    }

    void insert(int value, int index)
    {
        if(0==index)
        {
            addHead(value);

        }
        else if(index==(mSize-1))
        {
            addTail(value);
        }
        else
        {
            Node* newNode = new Node(value);
            Node* tmp = head;
            Node* prev = 0;
            int counter=0;

            while(counter!=index)
            {
                prev=tmp;
                tmp=tmp->next;
                counter++;
            }
            prev->next=newNode;
            newNode->next=tmp;
            mSize++;
        }
    }



};
