/*
 * Trabant.hpp
 *
 *  Created on: 27.03.2017
 *      Author: RENT
 */

#ifndef TRABANT_HPP_
#define TRABANT_HPP_
#include <iostream>
#include "Samochod.hpp"

class Trabant: public Samochod
{
protected:
	float mPojemnoscSilnika;
	std::string mMarka;
	std::string mKolor;

public:
	Trabant(std::string marka, std::string kolor, float poj);
	void kolor();
	void marka();
	void pojemnoscSilnika();
};



#endif /* TRABANT_HPP_ */
