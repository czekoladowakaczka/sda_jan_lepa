/*
 * Zuk.hpp
 *
 *  Created on: 27.03.2017
 *      Author: RENT
 */

#ifndef ZUK_HPP_
#define ZUK_HPP_
#include<iostream>
#include "Samochod.hpp"

class Zuk: public Samochod
{
protected:
	float mPojemnoscSilnika;
	std::string mMarka;
	std::string mKolor;

public:
	Zuk(std::string marka, std::string kolor, float poj);
	void kolor();
	void marka();
	void pojemnoscSilnika();
};



#endif /* ZUK_HPP_ */
