/*
 * Samochod.hpp
 *
 *  Created on: 27.03.2017
 *      Author: RENT
 */

#ifndef SAMOCHOD_HPP_
#define SAMOCHOD_HPP_

class Samochod
{
public:
	void virtual kolor() = 0;
	void virtual marka() = 0;
	void virtual pojemnoscSilnika() = 0;


	virtual ~Samochod()
	{
	}

};



#endif /* SAMOCHOD_HPP_ */
