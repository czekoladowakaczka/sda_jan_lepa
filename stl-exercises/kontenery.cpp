//============================================================================
// Name        : kontenery.cpp
// Author      : ghhh
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
#include <list>

using namespace std;

bool sortowanieIntowMalejace(const int& first, const int& second)
{
	return first>second;
}

bool stringPoZnakach(const string& first, const string& second)
{
	return first.size() < second.size();
}

bool greaterThenFive(const int& x)
{
	return x>5;
}

int main()
{
	list<int> list1;
	list1.push_back(2);
	list1.push_back(2);
	list1.push_front(0);
	list1.push_back(2);
	list1.push_back(1);
	list1.push_front(9);
	list1.push_back(111);
	list1.push_back(222);
	list1.push_back(768);

	list<int>list2;
	list2.push_back(11);
	list2.push_back(12);

	for(list<int>::iterator it = list1.begin(); it != list1.end(); ++it)
	{
//		kaaaazdy cyyyygan ma sliczneeeego pindoooola
//		cout<<list1.front()<<endl;
//		list1.pop_front();
		cout<<*it<<endl;
	}

	list1.remove(2);
	list1.remove_if(greaterThenFive);

	cout<<endl;

//	for(list<int>::iterator it = list2.begin(); it != list2.end(); ++it)
//	{
//		cout<<*it<<endl;
//	}
//	cout<<endl;

//	list2.splice(list2.end(), list1); //na poczatek listy2 ma dodajemy liste 1
//	list2.splice(list2.end(), list1, ++list1.begin()); //ostatni argument- mowi ktory argument z listy chcemy dodac
//	list2.splice(list2.end(), list1, ++list1.begin(), --list1.end()); //podaje do ktorego argumentu z drugiej listy kopiowac

	for(list<int>::iterator it = list1.begin(); it != list1.end(); ++it)
	{
		cout<<*it<<endl;
	}


//	for(list<int>::reverse_iterator it = list1.rbegin(); it != list1.rend(); ++it)
//		{
//			cout<<*it<<endl;
//		}
//
//	list<int>::iterator it = list1.begin();
//	list<int>::reverse_iterator itr = list1.rbegin();
//
//	bool palindrome=true;
//
//	while(it!=list1.end()||itr != list1.rend())
//	{
//		if(*it!=*itr)
//		{
//			palindrome=false;
//		}
//		++it;
//		++itr;
//	}
//
//	cout<<((palindrome==true) ? " " : " nie ")<<"jest palindromem"<<endl;


//	list<string>listString;
//	listString.push_back("dupa");
//	listString.push_back("DUPA");
//	listString.push_back("Andrzej");
//	listString.push_back("Duda");
//	listString.push_back("String");
//	listString.push_back("alalalalalalala");
//
//	for(list<string>::iterator it = listString.begin(); it != listString.end(); ++it)
//	{
//		cout<<*it<<endl;
//	}
//
//	listString.sort();
//
//	for (list<string>::iterator it = listString.begin(); it != listString.end(); ++it)
//	{
//		cout << *it << endl;
//	}




	return 0;
}
