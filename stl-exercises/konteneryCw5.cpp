//1. Napisz funkcj�, kt�ra przyjmuje stringa, nast�pnie szereguje wszystkie litery w porz�dku alfabetycznym i zwraca tak odwr�conego stringa (sort).
//2. Napisz analogiczn� funkcj�, kt�ra zwr�ci przemieszanego stringa (random_shuffle)
//3. Napisz funkcj�, kt�ra przyjmuje dwa stringi a nast�pnie zwraca vector ich wsp�lnych liter.
//4. Napisz funkcj�, kt�ra usunie spacje z podanego stringa(remove i erase na vector i list).
//5. Napisz funkcj�, kt�ra zliczy wyst�pienia podanej litery w podanym stringu (count).
//6. Napisz funkcj�, kt�ra sprawdzi czy podany string jest palindromem (reverse i transform)


#include <iostream>
#include <vector>
#include <list>
#include <algorithm>
#include <ctime>
#include <cstdlib>

using namespace std;

string sortString(string toSort)
{
	vector<char>notSorted;

	for(unsigned int i=0; i<toSort.size(); ++i)
	{
		notSorted.push_back(toSort[i]);
	}

	sort(notSorted.begin(), notSorted.end());

	return string(notSorted.begin(), notSorted.end());
}

vector<char> charInCommon(string first, string second)
{
	vector<char> inCommon;
	for(unsigned int i=0; i<first.size(); i++)
	{
		for(unsigned int k=0; k<second.size(); k++)
		{
			if(first[i]==second[k])
			{
				inCommon.push_back(second[k]);
			}
		}
	}

	sort(inCommon.begin(),inCommon.end());
	vector<char>::iterator it;
	it = unique(inCommon.begin(), inCommon.end());

	inCommon.resize(distance(inCommon.begin(),it));

	return inCommon;
}

void remove(string& removeFrom)
{
	vector<char>toChange;
	for(unsigned int i=0; i<removeFrom.size(); ++i)
	{
		toChange.push_back(removeFrom[i]);
	}

	for(vector<char>::iterator it=toChange.begin(); it!=toChange.end(); advance(it,1))
	{
		if(*it==' ')
		{
			toChange.erase(it);
		}
	}

	removeFrom= string(toChange.begin(), toChange.end());
}

void remove2(string& removeFrom)
{
	list<char>toChange;
	for(unsigned int i=0; i<removeFrom.size(); ++i)
	{
		toChange.push_back(removeFrom[i]);
	}

	toChange.remove(' ');

	removeFrom= string(toChange.begin(), toChange.end());
}

int countChar(string toCount, char thisChar)
{
	vector<char>toChange;
	for (unsigned int i = 0; i < toCount.size(); ++i)
	{
		toChange.push_back(toCount[i]);
	}

	return count(toChange.begin(),toChange.end(),thisChar);
}

int main()
{
	cout<<countChar("dududupa dlug gruz", 'd');

	return 0;
}
