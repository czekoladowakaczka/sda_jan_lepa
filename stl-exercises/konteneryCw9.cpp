//1. Napisz funkcj�, kt�ra przyjmuje stringa, nast�pnie uszereguje wszystkie litery w porz�dku alfabetycznym i zwraca tak odwr�conego stringa (sort).
//2. Napisz analogiczn� funkcj�, kt�ra zwr�ci przemieszanego stringa (random_shuffle). Zwr�� uwag� czy za ka�dym uruchomieniem, faktycznie string jest przemieszany innaczej
//3. Napisz funkcj�, kt�ra przyjmuje dwa stringi a nast�pnie zwraca vector ich wsp�lnych liter (powtarzajcych sie w obu stringach).
//4. Napisz funkcj�, kt�ra usunie spacje z podanego stringa. Zadanie wykonaj na dw�ch kontenrach: vector i list (remove i erase)
//5. Napisz funkcj�, kt�ra zliczy wyst�pienia podanej litery w podanym stringu (count).
//6. Napisz funkcj�, kt�ra sprawdzi czy podany string jest palindromem (reverse i transform lub equal)
//7. Napisz funkcj�, kt�ra stworzy wektor przechowuj�cy pot�gi kwadratowe z podanego zakresu np od. 3 do 10 (for_each)
//8. Zmie� poprzednie zadanie tak aby zwraca�o sum� kwadrat�w (accumulate)
//9. Napisz funkcj�, kt�ra stworzy wektor przechowuj�cy liczby od 1 do n. Nast�pnie utw�rz dwa wektory.
//Jeden, kt�rzy b�dzie przechowywa� tylko wielokrotno�ci 2, a drugi tylko wielokrotno�ci 3.
//Nast�pnie zwr�� vector przechowuj�cy tylko wielokrotno�ci 2 i 3. (remove_copy_if, set_intersection)
//10. Napisz funkcj�, kt�ra zwr�ci wszystkie mo�liwe permutacje 3 liczb (next_permutation)
//11. Napisz funkcj�, kt�ra przyjmuje wektor cyfr, a zwraca liczb� permutacji tych cyfr, kt�re s� wielokrotno�ci� 11.

#include <iostream>
#include <vector>
#include <list>
#include <algorithm>
#include <ctime>
#include <cstdlib>
#include <numeric>
//#include <functional>

using namespace std;

string sortString(string toSort)
{
	vector<char>notSorted;

	for(unsigned int i=0; i<toSort.size(); ++i)
	{
		notSorted.push_back(toSort[i]);
	}

	sort(notSorted.begin(), notSorted.end());

	return string(notSorted.begin(), notSorted.end());
}

vector<char> charInCommon(string first, string second)
{
	vector<char> inCommon;
	for(unsigned int i=0; i<first.size(); i++)
	{
		for(unsigned int k=0; k<second.size(); k++)
		{
			if(first[i]==second[k])
			{
				inCommon.push_back(second[k]);
			}
		}
	}

	sort(inCommon.begin(),inCommon.end());
	vector<char>::iterator it;
	it = unique(inCommon.begin(), inCommon.end());

	inCommon.resize(distance(inCommon.begin(),it));

	return inCommon;
}

void remove(string& removeFrom)
{
	vector<char>toChange;
	for(unsigned int i=0; i<removeFrom.size(); ++i)
	{
		toChange.push_back(removeFrom[i]);
	}

	for(vector<char>::iterator it=toChange.begin(); it!=toChange.end(); advance(it,1))
	{
		if(*it==' ')
		{
			toChange.erase(it);
			it--;
		}
	}

	removeFrom= string(toChange.begin(), toChange.end());
}

void remove2(string& removeFrom)
{
	list<char>toChange;
	for(unsigned int i=0; i<removeFrom.size(); ++i)
	{
		toChange.push_back(removeFrom[i]);
	}

	toChange.remove(' ');

	removeFrom= string(toChange.begin(), toChange.end());
}

int countChar(string toCount, char thisChar)
{
	vector<char>toChange;
	for (unsigned int i = 0; i < toCount.size(); ++i)
	{
		toChange.push_back(toCount[i]);
	}

	return count(toChange.begin(),toChange.end(),thisChar);
}

bool ifPalindrome(const string& toCheck)
{
	return equal(toCheck.begin(), toCheck.begin() + toCheck.size()/2, toCheck.rbegin());
}

void power(int &x)
{
	x=x*x;
}

vector<int> power2(int first, int last)
{
	vector<int>myVector;
	for(int i=first; i<=last; ++i)
	{
		myVector.push_back(i);
	}
	for_each(myVector.begin(), myVector.end(), power);

	return myVector;
}

int accumulatePower(int first, int last)
{
	vector<int>toSum=power2(first,last);
	return accumulate(toSum.begin(),toSum.end(),0);
}

bool ifNotMultipleOfTwo(int n)
{
	if(0==n%2)
	{
		return false;
	}
	return true;
}

bool ifNotMultipleOfThree(int n)
{
	if(0==n%3)
	{
		return false;
	}
	return true;
}

vector<int> ex9(int n)
{
	vector<int> myVector;

	for(int i=1; i<=n; ++i)
	{
		myVector.push_back(i);
	}
	vector<int> divBy2(myVector.size()); //remove_copy_if nie alokuje samodzielnie pamieci, wiec trzeba podac wielkosc w konstruktorze vectora, inaczej wywolanie funkcji spowoduje blad pamieci
	vector<int> divBy3(myVector.size());

	vector<int>::iterator it;

	it = remove_copy_if(myVector.begin(), myVector.end(), divBy2.begin(), ifNotMultipleOfTwo);
	divBy2.resize(distance(divBy2.begin(),it));

	it = remove_copy_if(myVector.begin(), myVector.end(), divBy3.begin(), ifNotMultipleOfThree);
	divBy3.resize(distance(divBy3.begin(),it));

	vector<int> divBy2and3(divBy2.size());

	it = set_intersection(divBy2.begin(),divBy2.end(),divBy3.begin(),divBy3.end(),divBy2and3.begin());

	divBy2and3.resize(distance(divBy2and3.begin(),it));

	return divBy2and3;
}


int main()
{
	vector<int>check;
	check = ex9(30);

	for(vector<int>::iterator it=check.begin(); it!=check.end(); advance(it,1))
	{
		cout<<*it<<" ";
	}



	return 0;
}
