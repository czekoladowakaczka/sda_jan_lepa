//1. Napisz funkcj�, kt�ra przyjmuje stringa, nast�pnie szereguje wszystkie litery w porz�dku alfabetycznym i zwraca tak odwr�conego stringa (sort).
//2. Napisz analogiczn� funkcj�, kt�ra zwr�ci przemieszanego stringa (random_shuffle)
//3. Napisz funkcj�, kt�ra przyjmuje dwa stringi a nast�pnie zwraca vector ich wsp�lnych liter.
//4. Napisz funkcj�, kt�ra usunie spacje z podanego stringa(remove i erase na vector i list).
//5. Napisz funkcj�, kt�ra zliczy wyst�pienia podanej litery w podanym stringu (count).
//6. Napisz funkcj�, kt�ra sprawdzi czy podany string jest palindromem (reverse i transform)


#include <iostream>
#include <vector>
#include <list>
#include <algorithm>
#include <ctime>
#include <cstdlib>

using namespace std;

string sortString(string toSort)
{
	vector<char>notSorted;

	for(unsigned int i=0; i<toSort.size(); ++i)
	{
		notSorted.push_back(toSort[i]);
	}

	sort(notSorted.begin(), notSorted.end());

	return string(notSorted.begin(), notSorted.end());
}

vector<char> charInCommon(string first, string second)
{
	vector<char> inCommon;
	for(unsigned int i=0; i<first.size(); i++)
	{
		for(unsigned int k=0; k<second.size(); k++)
		{
			if(first[i]==second[k])
			{
				inCommon.push_back(second[k]);
			}
		}
	}

	sort(inCommon.begin(),inCommon.end());
	vector<char>::iterator it;
	it = unique(inCommon.begin(), inCommon.end());

	inCommon.resize(distance(inCommon.begin(),it));

	return inCommon;
}


int main()
{
	string str1="anddrzej";
	string str2="dudajjjj";

	vector<char>inCommon = charInCommon(str1, str2);

	for(vector<char>::iterator it = inCommon.begin(); it!=inCommon.end(); advance(it,1))
	{
		cout<<*it;
	}

	return 0;
}
