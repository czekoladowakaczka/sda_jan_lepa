﻿//17. Napisz funkcję, który zliczy liczbę słów w podanym stringu oraz liczbę wystąpień każdego ze słów

#include <iostream>
#include <algorithm>
#include <string>
#include <list>
#include <map>

using namespace std;

list<string> stringToList(string toCheck)
{
    list<string>words;
    int spaceAmount = count(toCheck.begin(), toCheck.end(), ' ');
    int ptr = 0;
    string tmp;

    for(int i=0; i<=spaceAmount; ++i)
    {
        ptr = toCheck.find_first_of(' ');
        tmp = toCheck.substr(0, ptr);
        toCheck = toCheck.substr(ptr+1);
        if (tmp == "") continue;
        words.push_back(tmp);
    }
    return words;
}

void countWords(string toCheck)
{
    list<string>words = stringToList(toCheck);
    map<string, int> amountOfWords;
    pair<map<string,int>::iterator,bool> tmp;

    for(list<string>::iterator it=words.begin(); it!=words.end(); advance(it,1))
    {
        tmp = amountOfWords.insert(pair<string,int>(*it,1));
        if(!tmp.second)
        {
            tmp.first->second+=1;
        }
    }
    cout<<"Words in string: "<<words.size()<<endl;
    for(map<string,int>::iterator itMap = amountOfWords.begin();itMap!=amountOfWords.end(); advance(itMap,1))
    {
        cout<<"Word "<<itMap->first<<" x"<<itMap->second<<endl;
    }
}

int main()
{
   countWords("  ");

    return 0;
}
