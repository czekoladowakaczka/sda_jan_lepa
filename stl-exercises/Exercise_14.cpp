#include <iostream>
#include <vector>
#include <fstream>
#include <algorithm>

using namespace std;

struct Time
{
    int mHour;
    int mMinute;
    Time(int hour, int minute)
    :mHour(hour)
    ,mMinute(minute)
    {
    }
};

struct Course
{
    string mName;
    int mDay;
    Time mStart;
    Time mEnd;

    Course(string name, int day, int startHour, int startMinute, int endHour, int endMinute)
    :mName(name)
    ,mDay(day)
    ,mStart(startHour, startMinute)
    ,mEnd(endHour, endMinute)
    {
    }
};

void checkCollision(vector<Course>toCheck)
{
   for(vector<Course>::iterator it=toCheck.begin(); it!=toCheck.end(); advance(it, 1))
   {
       for(vector<Course>::iterator it2=toCheck.begin()+1; it2!=toCheck.end(); advance(it2, 1))
       {
            if(it->mDay==it2->mDay)
            {
                if((it->mStart.mHour>it2->mStart.mHour&&it->mStart.mHour<it2->mEnd.mHour)||(it->mEnd.mHour>it2->mStart.mHour&&it->mEnd.mHour<it2->mEnd.mHour));//||(it->mStart.mHour==it2->mStart.mHour)||(it->mEnd.mHour==it2->mEnd.mHour));
                {
                    cout<<"Siem naklada!"<<endl;
                }
            }
       }
   }
}

bool sortByDay(Course first, Course second)
{
    return first.mDay<second.mDay;
}

int main()
{
    vector<Course> courses;

    fstream file;
    file.open("dane.txt");

    string name;
    int day;
    int startHour;
    int startMinute;
    int endHour;
    int endMinute;

    while(!file.eof())
    {
        file>>name>>day>>startHour>>startMinute>>endHour>>endMinute;
        courses.push_back(Course(name,day, startHour, startMinute, endHour, endMinute));
    }

    sort(courses.begin(), courses.end(), sortByDay);

    for(vector<Course>::iterator it=courses.begin(); it!=courses.end(); advance(it, 1))
    {
        cout<<it->mName<<" "<<it->mDay<<" "<<it->mStart.mHour<<" "<<it->mStart.mMinute<<" "<<it->mEnd.mHour<<" "<<it->mEnd.mMinute<<endl;
    }

    checkCollision(courses);

    return 0;
}
