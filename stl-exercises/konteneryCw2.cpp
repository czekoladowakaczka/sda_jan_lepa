
//1. Napisz funkcj�, kt�ra przyjmuje stringa, nast�pnie szereguje wszystkie litery w porz�dku alfabetycznym i zwraca tak odwr�conego stringa (sort).
//2. Napisz analogiczn� funkcj�, kt�ra zwr�ci przemieszanego stringa (random_shuffle)
//3. Napisz funkcj�, kt�ra przyjmuje dwa stringi a nast�pnie zwraca vector ich wsp�lnych liter.
//4. Napisz funkcj�, kt�ra usunie spacje z podanego stringa(remove i erase na vector i list).
//5. Napisz funkcj�, kt�ra zliczy wyst�pienia podanej litery w podanym stringu (count).
//6. Napisz funkcj�, kt�ra sprawdzi czy podany string jest palindromem (reverse i transform)


#include <iostream>
#include <vector>
#include <list>
#include <algorithm>
#include <ctime>
#include <cstdlib>

using namespace std;

string sortString(string toSort)
{
	vector<char>notSorted;

	for(unsigned int i=0; i<toSort.size(); ++i)
	{
		notSorted.push_back(toSort[i]);
	}

	sort(notSorted.begin(), notSorted.end());

	return string(notSorted.begin(), notSorted.end());
}


string shuffleString(string toShuffle)
{
	vector<char>notSorted;

	for(unsigned int i=0; i<toShuffle.size(); ++i)
	{
		notSorted.push_back(toShuffle[i]);
	}

	random_shuffle(notSorted.begin(), notSorted.end());

	return string(notSorted.begin(), notSorted.end());
}

int main()
{
	srand(time(NULL));

	string str1="andrzej";
	cout<<sortString(str1)<<endl;
	cout<<shuffleString(str1)<<endl;
	cout<<shuffleString(str1)<<endl;
	cout<<shuffleString(str1)<<endl;


	return 0;
}
