/*
 * main.cpp
 *
 *  Created on: 26.04.2017
 *      Author: RENT
 */
#include <iostream>
#include "Figura.hpp"
#include "Kolor.hpp"
#include "Kolo.hpp"
#include "Kwadrat.hpp"


int main()
{

//	Color::Kolor kolor(Color::czarny);
//	std::cout<<Color::convertToString(kolor);

	//Figura* kwadrat("kwadrat", 2.5, Color::czarny);

	Kwadrat kwadrat("kwadrat", 2.5, Color::CZARNY);
//	kwadrat.wypisz();
//	Kolo kolo("kolo", 3, Color::BIALY);
//	kolo.wypisz();

	Figura* wskaz;
	wskaz = &kwadrat;
	wskaz->wypisz();

	return 0;
}
