/*
 * Kwadrat.hpp
 *
 *  Created on: 26.04.2017
 *      Author: RENT
 */

#ifndef KWADRAT_HPP_
#define KWADRAT_HPP_
#include <iostream>
#include "Figura.hpp"
#include "Kolor.hpp"

class Kwadrat: public Figura
{
public:
	Kwadrat(std::string nazwa, float bok, Color::Kolor kolor);
	void wypisz();

protected:
	std::string mNazwa;
	float mDlugoscBoku;
	Color::Kolor mKolor;
};



#endif /* KWADRAT_HPP_ */
