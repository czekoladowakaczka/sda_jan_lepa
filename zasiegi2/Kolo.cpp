/*
 * Kolo.cpp
 *
 *  Created on: 26.04.2017
 *      Author: RENT
 */
#include "Kolo.hpp"

Kolo::Kolo(std::string nazwa, float promien, Color::Kolor kolor)
:mNazwa(nazwa)
,mPromien(promien)
,mKolor(kolor)
{}

void Kolo::wypisz()
{
	std::cout<<"Nazwa figury "<<mNazwa<<std::endl;
	std::cout<<"Promien "<<mPromien<<std::endl;
	std::cout<<"Kolor "<<Color::convertToString(mKolor)<<std::endl;
}
