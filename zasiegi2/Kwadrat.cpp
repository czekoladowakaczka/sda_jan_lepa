/*
 * Kwadrat.cpp
 *
 *  Created on: 26.04.2017
 *      Author: RENT
 */
#include "Kwadrat.hpp"


Kwadrat::Kwadrat(std::string nazwa, float bok, Color::Kolor kolor)
:mNazwa(nazwa)
,mDlugoscBoku(bok)
,mKolor(kolor)
{}

void Kwadrat::wypisz()
{
	std::cout<<"Nazwa figury "<<mNazwa<<std::endl;
	std::cout<<"Dlugosc boku "<<mDlugoscBoku<<std::endl;
	std::cout<<"Kolor "<<Color::convertToString(mKolor)<<std::endl;
}

