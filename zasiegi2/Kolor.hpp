/*
 * Kolor.hpp
 *
 *  Created on: 26.04.2017
 *      Author: RENT
 */

#ifndef KOLOR_HPP_
#define KOLOR_HPP_
#include <string>

namespace Color
{

	enum Kolor
	{
		CZARNY=0,
		BIALY,
		NIEBIESKI
	};

	std::string convertToString(Kolor kolor);
}



#endif /* KOLOR_HPP_ */
