/*
 * Kolo.hpp
 *
 *  Created on: 26.04.2017
 *      Author: RENT
 */

#ifndef KOLO_HPP_
#define KOLO_HPP_
#include <iostream>
#include "Figura.hpp"
#include "Kolor.hpp"

class Kolo: public Figura
{
public:
	Kolo(std::string nazwa, float promien, Color::Kolor kolor);
	void wypisz();

protected:
	std::string mNazwa;
	float mPromien;
	Color::Kolor mKolor;
};


#endif /* KOLO_HPP_ */
