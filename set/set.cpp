//============================================================================
// Name        : set.cpp
// Author      : ghhh
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
#include<set>

using namespace std;

struct compareIntegralPart
{
	bool operator() (float first, float second) const
	{
		return static_cast<int>(first) < static_cast<int>(second);
	}
};

int main() 
{

	set<string> imiona;
	string tabString[]={"Alfa", "Delta", "Charlie", "Brawo"};
imiona.insert(tabString, tabString+3); imiona.insert("Ala");	imiona.insert("B");	imiona.insert("C");	imiona.insert("D");	imiona.insert("AndrzejDuda");	for(set<string>::iterator it = imiona.begin(); it!= imiona.end(); ++it)
	{cout<<*it<<endl;
}

	set<float, compareIntegralPart> liczby;

	liczby.insert(1.1);
	liczby.insert(2.2);
	liczby.insert(3.5);
	liczby.insert(4.4);
	liczby.insert(4.44);
	liczby.insert(5.44);

	for (set<float>::iterator it = liczby.begin(); it != liczby.end(); ++it)
	{
		cout << *it << endl;
	}

	set<float>::iterator it;
	it = liczby.find(3.5);
	liczby.erase(it);

	for(set<float>::iterator it = liczby.begin(); it!= liczby.end(); ++it)
	{
		cout<<*it<<endl;
	}


	return 0;
}
