/*
 * Ser.hpp
 *
 *  Created on: 20.05.2017
 *      Author: RENT
 */

#ifndef SER_HPP_
#define SER_HPP_
#include <iostream>

class Ser
{
double mCena;

public:
	virtual void podajCene()
	{
		std::cout<<"Cena "<<mCena<<std::endl;
	}

	Ser(double cena)
	:mCena(cena)
	{
	}

	Ser()
	:mCena(0)
	{
	}

	virtual ~Ser()
	{
	}
};



#endif /* SER_HPP_ */
