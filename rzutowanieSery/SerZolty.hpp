/*
 * SerZolty.hpp
 *
 *  Created on: 20.05.2017
 *      Author: RENT
 */

#ifndef SERZOLTY_HPP_
#define SERZOLTY_HPP_
#include "Ser.hpp"
#include <iostream>

class SerZolty: public Ser
{
	double mWiek;

public:
	void podajWiek();
	SerZolty(double cena, double wiek);
	SerZolty();
};




#endif /* SERZOLTY_HPP_ */
