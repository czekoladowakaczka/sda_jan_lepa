/*
 * SerBialy.hpp
 *
 *  Created on: 20.05.2017
 *      Author: RENT
 */

#ifndef SERBIALY_HPP_
#define SERBIALY_HPP_
#include <iostream>
#include "Ser.hpp"

class SerBialy: public Ser
{
public:
	enum Rodzaj
	{
		Chudy,
		Tlusty,
		Poltlusty
	};

	std::string podajRodzaj();
	void wypiszRodzaj();
	SerBialy(double cena, Rodzaj rodzaj);
	SerBialy();

public:
	Rodzaj mRodzaj;
};




#endif /* SERBIALY_HPP_ */
