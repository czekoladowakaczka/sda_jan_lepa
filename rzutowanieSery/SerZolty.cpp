/*
 * SerZolty.cpp
 *
 *  Created on: 20.05.2017
 *      Author: RENT
 */

#include "SerZolty.hpp"

void SerZolty::podajWiek()
{
	std::cout<<"Ser ma "<<mWiek<<std::endl;
}


SerZolty::SerZolty(double cena, double wiek)
:Ser(cena)
,mWiek(wiek)
{
}

SerZolty::SerZolty()
:Ser()
,mWiek(0)
{
}
