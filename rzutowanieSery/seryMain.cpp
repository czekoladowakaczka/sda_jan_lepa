/*
 * seryMain.cpp
 *
 *  Created on: 20.05.2017
 *      Author: RENT
 */

#include <iostream>
#include "Ser.hpp"
#include "SerBialy.hpp"
#include "SerZolty.hpp"

using namespace std;

void sertuj(Ser** tab, int rozmiar)
{
	SerBialy** bialaTab = new SerBialy*[rozmiar];
	SerZolty** zoltaTab = new SerZolty*[rozmiar];
	int ileBialych = 0;
	int ileZoltych = 0;

	SerBialy* bialyTmp = 0;
	SerZolty* zoltyTmp = 0;

	for(int i=0; i<rozmiar; i++)
	{
		bialyTmp = dynamic_cast<SerBialy*>(tab[i]);
		if(bialyTmp)
		{
			bialaTab[ileBialych] = bialyTmp;
			ileBialych++;
			continue;
		}

		zoltyTmp = dynamic_cast<SerZolty*>(tab[i]);
	    if(zoltyTmp)
		{
			zoltaTab[ileZoltych] = zoltyTmp;
			ileZoltych++;
		}
	}

	for(int i=0; i<ileBialych; ++i)
	{
		cout<<"Bialy"<<endl;
		bialaTab[i]->podajCene();
		bialaTab[i]->podajRodzaj();
		cout<<endl;
	}

	for(int i=0; i<ileZoltych; ++i)
	{
		cout<<"Zolty"<<endl;
		zoltaTab[i]->podajCene();
		zoltaTab[i]->podajWiek();
		cout<<endl;
	}

	delete[] bialaTab;
	delete[] zoltaTab;
}



int main()
{
	Ser* sery[8];

	sery[0] = new SerZolty(10, 10);
	sery[1] = new SerBialy(2, SerBialy::Tlusty);
	sery[2] = new SerZolty(3, 10);
	sery[3] = new SerBialy(4, SerBialy::Chudy);
	sery[4] = new SerZolty(5, 8);
	sery[5] = new SerBialy(6, SerBialy::Poltlusty);
	sery[6] = new SerZolty(7, 8);
	sery[7] = new SerBialy(8, SerBialy::Chudy);

	sertuj(sery,8);



	for(int i=0; i<8; i++)
	{
		delete sery[i];
	}

	return 0;
}
