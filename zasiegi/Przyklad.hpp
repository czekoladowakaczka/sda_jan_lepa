/*
 * Przyklad.hpp
 *
 *  Created on: 26.04.2017
 *      Author: RENT
 */

#ifndef PRZYKLAD_HPP_
#define PRZYKLAD_HPP_
#include <string>

extern int licznik;
extern float liczba;
extern std::string napis;

void zmienString(std::string x);
void zmienFloat(float x);
void pokazString();
void pokazFloat();


#endif /* PRZYKLAD_HPP_ */
