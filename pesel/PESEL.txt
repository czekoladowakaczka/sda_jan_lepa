
//Wczytaj PESEL u�ytkownika
//
//    We� pierwsze 10 cyfr numeru
//    Pomn� ka�d� z cyfr przez odpowiedni� wag�: 1, 3, 7, 9, 1, 3, 7, 9, 1, 3
//    Oblicz sum� tych iloczyn�w
//    Oblicz reszt� z dzielenia przez 10 i odejmij j� od 10
//    Je�li wynik jest r�wny 11. cyfrze numeru PESEL, to numer jest poprawny
//
//Wy�wietl informacj� o poprawno��i numeru u�ytkownikowi

#include <iostream>

using namespace std;

int checkPesel(int *arrayPesel, int *arrayWages)
{
	int sum=0;
	int last=0;

	for(int i=0; i<10; ++i)
	{
		arrayPesel[i]=arrayPesel[i]*arrayWages[i];
		sum+=arrayPesel[i];
	}

	last = 10-(sum%10);

	return last;
}

int main()
{
	string pesel;
	cout<<"Podaj pesel: ";
	cin>>pesel;
	int arrayPesel[11];
	int arrayWages[]={1, 3, 7, 9, 1, 3, 7, 9, 1, 3 };

	if(pesel.length()!=11)
	{
		cout<<"Incorrect pesel";
		return 0;
	}

	for(int i=0; i<11 ;++i)
	{
		arrayPesel[i]=pesel[i]-'0';
	}

	if(checkPesel(arrayPesel, arrayWages))
    {
        cout<<"Correct pesel";
    }
    else
    {
        cout<<"Incorrect pesel";
    }

	return 0;
}