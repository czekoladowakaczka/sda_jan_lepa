//22. Klasa NIEabstrakcyjna bazowa Animal (przechowuj�ca inta - numer seryjny) + dwie klasy pochodne Dog,
//	Bird przeci�zaj�ce wirtualna metod� whoAreYou() -> wypisujaca nazw� klasy.
//    Stworzy� kontener na klas� Animal i wype�ni� go losowo 200 obiektami Dog lub Bird nadaj�c im kolejne numery seryjne.
//	Nast�pnie wywo�a� na ka�dym z element�w metod� whoAreYou.
//    Rodzieli� Dog od Bird na dwia osobne kontenery i ponownie wywolac metod� whoAreYou().

//kontener musi byc na wskazniki(vector na animale), nowe wrzucamy za pomoca NEW
//dynamic_cast

#include <iostream>
#include <vector>
#include <ctime>
#include <cstdlib>
#include <algorithm>

using namespace std;

class Animal
{
	int mSerialNumber;

public:
	virtual void  whoAreYou()
	{
		cout<<"Jestem zwierzem!"<<endl;
	}
	Animal(int serialNumber)
	:mSerialNumber(serialNumber)
	 {}

	virtual ~Animal()
	{}
};

class Dog: public Animal
{
public:
	void  whoAreYou()
	{
		cout<<"Jestem psem!"<<endl;
	}

	Dog(int serialNumber)
	:Animal(serialNumber)
	{}

	virtual ~Dog()
	{}
};

class Bird: public Animal
{
public:
	void  whoAreYou()
	{
		cout<<"Jestem ptakiem!"<<endl;
	}

	Bird(int serialNumber)
	:Animal(serialNumber)
	{}

	virtual ~Bird()
	{}
};

void showType(Animal* animal)
{
	animal->whoAreYou();
}

int main()
{
	srand(time(NULL));
	vector<Animal*> animals;
	int animalType;
	int serialNumber = 1;

	for(int i=0; i<200; ++i)
	{
		 animalType = rand()%2;
		 if(0==animalType)
		 {;
			 animals.push_back(new Dog(serialNumber));
		 }
		 else if(1==animalType)
		 {
			 animals.push_back(new Dog(serialNumber));
		 }
		 serialNumber++;
	}

//	for_each(animals.begin(), animals.end(), showType);

	Bird* tempBird = 0;
	Dog* tempDog = 0;

	vector<Dog*> dogs;
	vector<Bird*> birds;

	for(vector<Animal*>::iterator it = animals.begin(); it!=animals.end(); advance(it,1))
	{
		tempDog = dynamic_cast<Dog*>(*it);

		if(tempDog)
		{
			dogs.push_back(tempDog);
		}
		tempBird = dynamic_cast<Bird*>(*it);

		if (tempBird)
		{
			birds.push_back(tempBird);
		}
	}

	for_each(dogs.begin(),dogs.end(),showType);
	for_each(birds.begin(),birds.end(),showType);

	for(vector<Animal*>::iterator it = animals.begin(); it!=animals.end(); advance(it,1))
	{
		delete *it;
	}

	return 0;
}
