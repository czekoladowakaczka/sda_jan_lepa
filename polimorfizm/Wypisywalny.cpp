/*
 * Wypisywalny.cpp
 *
 *  Created on: 30.03.2017
 *      Author: RENT
 */

#include "Wypisywalny.hpp"

Wypisywalny::Wypisywalny()
:mLancuch("dupa")
{
	std::cout<<"Tworz�: "<<mLancuch<<std::endl;
}

Wypisywalny::Wypisywalny(std::string lancuch)
:mLancuch(lancuch)
{
	std::cout<<"Tworz�: "<<mLancuch<<std::endl;
}

Wypisywalny::~Wypisywalny()
{
	std::cout<<"Niszcz� : "<<mLancuch<<std::endl;
}

const std::string& Wypisywalny::getLancuch() const
{
	return mLancuch;
}

void Wypisywalny::setLancuch(const std::string& lancuch)
{
	mLancuch = lancuch;
}

void Wypisywalny::wypisz()
{
	std::cout<<"Lancuch: "<<mLancuch<<std::endl;
}
