/*
 * PodstawieniowyTekst.hpp
 *
 *  Created on: 30.03.2017
 *      Author: RENT
 */

#ifndef PODSTAWIENIOWYTEKST_HPP_
#define PODSTAWIENIOWYTEKST_HPP_
#include <iostream>
#include "SzyfrowanyTekst.hpp"

class PodstawieniowyTekst: public SzyfrowanyTekst
{
public:
	PodstawieniowyTekst();
	PodstawieniowyTekst(std::string a);
	~PodstawieniowyTekst();
	void szyfruj();
	void wypisz();
};



#endif /* PODSTAWIENIOWYTEKST_HPP_ */
