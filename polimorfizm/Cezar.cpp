/*
 * Cezar.cpp
 *
 *  Created on: 30.03.2017
 *      Author: RENT
 */
#include "Cezar.hpp"

CezarTekst::CezarTekst(std::string a)
{
	mLancuch = a;
	std::cout<<"Konstruktor cezara"<<std::endl;
}

CezarTekst::CezarTekst()
{
	mLancuch = "abcd";
	std::cout<<"Konstruktor cezara"<<std::endl;
}

CezarTekst::~CezarTekst()
{
	std::cout<<"Destruktor cezara"<<std::endl;
}

void CezarTekst::szyfruj()
{
	for(unsigned int i=0; i<mLancuch.length(); ++i)
	{
		mLancuch[i]=mLancuch[i]+3;
	}
	mCzyZaszyfrowane = true;
}

void CezarTekst::wypisz()
{
	std::cout<<"Cezar tekst: "<<mLancuch<<std::endl;
}
