/*
 * Cezar.hpp
 *
 *  Created on: 30.03.2017
 *      Author: RENT
 */

#ifndef CEZAR_HPP_
#define CEZAR_HPP_
#include "SzyfrowanyTekst.hpp"

class CezarTekst: public SzyfrowanyTekst //virtual czy public?
{
public:
	CezarTekst(std::string a);
	CezarTekst();
	~CezarTekst();
	void szyfruj();
	void wypisz();
};



#endif /* CEZAR_HPP_ */
