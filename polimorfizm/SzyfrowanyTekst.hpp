/*
 * SzyfrowanyTekst.hpp
 *
 *  Created on: 30.03.2017
 *      Author: RENT
 */

#ifndef SZYFROWANYTEKST_HPP_
#define SZYFROWANYTEKST_HPP_
#include "Wypisywalny.hpp"

class SzyfrowanyTekst: public Wypisywalny
{
public:
	virtual void szyfruj() = 0;
	virtual void wypisz() = 0;

	SzyfrowanyTekst()
	:mCzyZaszyfrowane(0)
	{
		std::cout<<"Konstruktor szyfrowanego tekstu"<<std::endl;
	}

	virtual ~SzyfrowanyTekst()
	{
		std::cout<<"Destruktor szyfrowanego tekstu"<<std::endl;
	}
protected:
	bool mCzyZaszyfrowane;
};


#endif /* SZYFROWANYTEKST_HPP_ */
