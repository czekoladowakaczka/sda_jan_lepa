/*
 * main.cpp
 *
 *  Created on: 30.03.2017
 *      Author: RENT
 */

#include <iostream>
#include "Cezar.hpp"
#include "PodstawieniowyTekst.hpp"

int main()
{
	CezarTekst c1("cezar1");
	CezarTekst c2("cezar2");
	PodstawieniowyTekst p3("podst1");
	PodstawieniowyTekst p4("podst2");
	SzyfrowanyTekst *tablica[4];
	tablica[0] =  &c1;
	tablica[1] = &c2;
	tablica[2] = &p3;
	tablica[3] = &p4;

	for(int i=0; i<4; ++i)
	{
		tablica[i]->szyfruj();
		tablica[i]->wypisz();
	}

	return 0;
}
