/*
 * Wypisywalny.hpp
 *
 *  Created on: 30.03.2017
 *      Author: RENT
 */

#ifndef WYPISYWALNY_HPP_
#define WYPISYWALNY_HPP_
#include <iostream>

class Wypisywalny
{
public:
	Wypisywalny();
	Wypisywalny(std::string lancuch);
	~Wypisywalny();

	const std::string& getLancuch() const;
	void setLancuch(const std::string& lancuch);
	void wypisz();

protected:
	std::string mLancuch;
};




#endif /* WYPISYWALNY_HPP_ */
