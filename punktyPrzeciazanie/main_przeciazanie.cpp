/*
 * main_przeciazanie.cpp
 *
 *  Created on: 06.05.2017
 *      Author: RENT
 */
#include<iostream>
#include"Punkt.hpp"
#include"Zbior.hpp"

using namespace std;

int main()
{
	Punkt p1(2,3);
	Punkt p2=p1;

	Punkt p3=++p2+3;

	Zbior zbior;

	if(zbior)
	{
		std::cout<<"Prawidlowy"<<endl;
	}
	else
	{
		std::cout<<"Nieprawdidlowy"<<endl;
	}

	zbior + p1;
	zbior + p2;
	zbior + p3;
	zbior.wypisz();

	Zbior zbiornik;
	zbiornik + Punkt(33,33);
	zbiornik + Punkt(11,11);
	zbior + zbiornik;
	zbior.wypisz();



//	float x=2;

//	p1.wypisz();
//	p1=p1*x;
//	p1.wypisz();
//
//	p1.wypisz();
//	p1=p1+p2;
//	p1.wypisz();
//	p1=p1+x;
//	p1.wypisz();
//	p1=x*p1;
//	p1.wypisz();
//	p1=x+p1;
//	p1.wypisz();






	return 0;
}
