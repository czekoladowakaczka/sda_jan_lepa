//============================================================================
// Name        : strukturki.cpp
// Author      : ghhh
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
using namespace std;

struct Czlowiek
{
	Czlowiek(int dzien, int miesiac, int rok)
	:mDzien(dzien)
	,mMiesiac(miesiac)
	,mRok(rok)
	{
	}

	int podajWiek(int wiek)
	{
		return 2017-wiek;
	}

	int mDzien;
	int mMiesiac;
	int mRok;
};

struct Pracownik: Czlowiek
{
	float mPensja;
	int mStaz;

	void wyliczPensje()
	{
		mPensja *= (1+ (++mStaz)*0.05f);
	}
};

struct Student: Czlowiek
{

private:
	int mNumerIndeksu;

public:
	Student(int dzien, int miesiac, int rok, int nrindeksu)
	:Czlowiek::Czlowiek(dzien,miesiac,rok)
	,mNumerIndeksu(nrindeksu)
	{
	}

	void bawSie()
	{
		std::cout<<"Akademicki melanz do krwi"<<std::endl;
	}
};

int main() {

	Student andzej(12,1,1900, 1234);
	andzej.bawSie();

	return 0;
}
