/*
 * AbstractSemiconductorFactory.hpp
 *
 *  Created on: 08.04.2017
 *      Author: RENT
 */

#ifndef ABSTRACTSEMICONDUCTORFACTORY_HPP_
#define ABSTRACTSEMICONDUCTORFACTORY_HPP_


class AbstractSemiconducorFactory
{
public:
	virtual Processor* createProcessor()=0;
	virtual Cooler* createCooler()=0;
	virtual ~AbstractSemiconducorFactory()
	{}
};


#endif /* ABSTRACTSEMICONDUCTORFACTORY_HPP_ */
