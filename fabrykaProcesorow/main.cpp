/*
 * main.cpp
 *
 *  Created on: 08.04.2017
 *      Author: RENT
 */
#include "Computer.hpp"
#include "IntelsemiconductorFactory.hpp"
#include "AMDsemiconductorFactory.hpp"



int main()
{
	IntelSemiconducorFactory intelFactory;
	AMDSemiconducorFactory amdFactory;

	Computer intelPC("PC1", intelFactory);
	Computer amdPC("PC2", amdFactory);

	intelPC.run();
	amdPC.run();
}
