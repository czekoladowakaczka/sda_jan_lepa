/*
 * Computer.hpp
 *
 *  Created on: 08.04.2017
 *      Author: RENT
 */

#ifndef COMPUTER_HPP_
#define COMPUTER_HPP_
#include <iostream>
#include "Processor.hpp"
#include "Cooler.hpp"
#include "ProcessorAMD.hpp"
#include "CoolerAMD.hpp"
#include "ProcessorIntel.hpp"
#include "CoolerINTEL.hpp"
#include "AbstractSemiconductorFactory.hpp"

class Computer
{
public:

	Computer(std::string name, AbstractSemiconducorFactory& factory)
	:mName(name)
	{
		mProcessor = factory.createProcessor();
		mCooler = factory.createCooler();
	}

	~Computer()
	{
		delete mProcessor;
		delete mCooler;
	}
	void run()
	{
		mProcessor->process();
		mCooler->cool();
	}
protected:
	std::string mName;
	Processor* mProcessor;
	Cooler* mCooler;
};



#endif /* COMPUTER_HPP_ */
