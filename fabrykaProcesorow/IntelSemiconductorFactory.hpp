/*
 * IntelSemiconductorFactory.hpp
 *
 *  Created on: 08.04.2017
 *      Author: RENT
 */

#ifndef INTELSEMICONDUCTORFACTORY_HPP_
#define INTELSEMICONDUCTORFACTORY_HPP_
#include "AbstractSemiconductorFactory.hpp"

class IntelSemiconducorFactory: public AbstractSemiconducorFactory
{
	Processor* createProcessor()
	{
		return new ProcessorIntel();
	}
	Cooler* createCooler()
	{
		return new CoolerINTEL();
	}
};



#endif /* INTELSEMICONDUCTORFACTORY_HPP_ */
