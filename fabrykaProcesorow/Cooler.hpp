/*
 * Cooler.hpp
 *
 *  Created on: 08.04.2017
 *      Author: RENT
 */

#ifndef COOLER_HPP_
#define COOLER_HPP_

class Cooler
{
public:
	virtual void cool()=0;
	virtual ~Cooler()
	{}
};



#endif /* COOLER_HPP_ */
