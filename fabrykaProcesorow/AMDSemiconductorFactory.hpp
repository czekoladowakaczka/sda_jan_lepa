/*
 * AMDSemiconductorFactory.hpp
 *
 *  Created on: 08.04.2017
 *      Author: RENT
 */

#ifndef AMDSEMICONDUCTORFACTORY_HPP_
#define AMDSEMICONDUCTORFACTORY_HPP_
#include "AbstractSemiconductorFactory.hpp"

class AMDSemiconducorFactory: public AbstractSemiconducorFactory
{
	Processor* createProcessor()
	{
		return new ProcessorAMD;
	}
	Cooler* createCooler()
	{
		return new CoolerAMD();
	}
};


#endif /* AMDSEMICONDUCTORFACTORY_HPP_ */
