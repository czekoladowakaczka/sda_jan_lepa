/*
 * Processor.hpp
 *
 *  Created on: 08.04.2017
 *      Author: RENT
 */

#ifndef PROCESSOR_HPP_
#define PROCESSOR_HPP_

class Processor
{
public:
	Processor()
	{}

	virtual void process()=0;
	virtual ~Processor()
	{}
};



#endif /* PROCESSOR_HPP_ */
